# LevenshteinJS

LevenshteinJS is a JavaScript implementation of the Levenshtein distance algorithm.

It is used to find the minimum number of single-character substitutions, insertions, and deletions required to transform one string into another.

Some potential applications include providing spell check and autocomplete suggestions as well as assisting with plagiarism detection.

## Usage

To use LevenshteinJS, simply include a reference to levenshtein.js in your project, as seen below. No dependencies required.

```html
<script src="levenshtein.js"></script>
```

Example usage:

```JavaScript
levenshtein = new Levenshtein();
var distance = levenshtein.getDistance('test', 'txst');
```

`levenshtein.compare` is available as an alias for the getDistance function.

## Running Unit Tests

To run the unit tests, open spec-runner.html in a browser.

## Authors

- **Dustin Farley** - _Maintainer_ - [dcfarley](https://gitlab.com/dcfarley)

See also the list of [contributors](https://gitlab.com/dcfarley/levenshtein-js/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/dcfarley/levenshtein-js/blob/master/LICENSE) file for details.
