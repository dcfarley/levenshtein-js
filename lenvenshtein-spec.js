describe('Levenshtein', () => {
  beforeEach(function() {
    levenshtein = new Levenshtein();
  });
  describe('getDistance', () => {
    it('should find necessary insertion', () => {
      var distance = levenshtein.getDistance('test', 'test2');
      expect(distance).toBe(1);
    });

    it('should find necessary deletion', () => {
      var distance = levenshtein.getDistance('test', 'tes');
      expect(distance).toBe(1);
    });

    it('should find necessary substition', () => {
      var levenshtein = new Levenshtein();
      var distance = levenshtein.getDistance('test', 'txst');
      expect(distance).toBe(1);
    });

    it('should find multiple differences', () => {
      var levenshtein = new Levenshtein();
      var distance = levenshtein.getDistance('saturday', 'sunday');
      expect(distance).toBe(3);
    });
  });
  describe('compare', () => {
    it('should be alias for getDistance', () => {
      expect(levenshtein.compare).toEqual(levenshtein.getDistance);
    });
  });
});
