function Levenshtein() {}

Levenshtein.prototype.getDistance = function(source, target) {
  if (source.length === 0) {
    return target.length;
  }

  if (target.length === 0) {
    return source.length;
  }

  source = source.toLowerCase();
  target = target.toLowerCase();

  var matrix = [];
  var subCost = 0;

  for (var i = 0; i <= source.length; i++) {
    matrix[i] = [];
    for (var j = 0; j <= target.length; j++) {
      if (i === 0) {
        matrix[i][j] = j;
      } else if (j === 0) {
        matrix[i][j] = i;
      } else {
        if (source.charAt(i - 1) === target.charAt(j - 1)) {
          subCost = 0;
        } else {
          subCost = 1;
        }

        matrix[i][j] = Math.min(
          matrix[i - 1][j] + 1,
          matrix[i][j - 1] + 1,
          matrix[i - 1][j - 1] + subCost
        );
      }
    }
  }

  return matrix[source.length][target.length];
};

Levenshtein.prototype.compare = Levenshtein.prototype.getDistance;
